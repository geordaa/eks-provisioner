# Provision an EKS Cluster

This repo is a repo containing Terraform configuration files to provision an EKS cluster on AWS.

After installing the AWS CLI. Configure it to use your credentials.

```shell
$ aws configure
AWS Access Key ID [None]: <YOUR_AWS_ACCESS_KEY_ID>
AWS Secret Access Key [None]: <YOUR_AWS_SECRET_ACCESS_KEY>
Default region name [None]: <YOUR_AWS_REGION>
Default output format [None]: json
```

You will need to create a s3 bucket in whicch to store the terraform state file to the backend. 

Create a bucket named such as "geordaa-terraform-backend-store". Replace "geordaa" with your own identifier to make the bucket name unique. Next edit kubernetes/backend.tf to reflect your bucket name.

This project also uses terraform workspaces. Look at the contents of .gitlab-ci.yml
If a CI/CD pipeline is run from GitLab using the master branch, a workspace named "prod" will be used. A workspace will be created from the branch name for any non-master deployments

If using GitLab CI, don't forget to add your AWS credentials to the pipeline configuration in Settings/CICD/Variables

<img src="../img/variables.png" align="center" width="600" alt="Variables">

The s3 bucket will be partitioned by workspace, so the contents of s3 will have the following notation:
s3://geordaa-terraform-backend-store/env:/**prod**/kubernetes/

The default for the project deployment is us-west-2. Edit vpc.tf if another region is required.

A new VPC is created called "training.vpc". An EKS cluster is created within this VPC using K8s 1.21
Istio 1.10 is used. The commands are laid out in the readme for the Kubernetes folder, and an istio installer shell script for easy installation exists

There is a setup.sh that carries out some installation tasks after the EKS cluster has been created. This is not a comprehensive setup, but rather an example to get you started.
