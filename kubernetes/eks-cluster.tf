module "eks" {
  source       = "terraform-aws-modules/eks/aws"
  cluster_name = local.cluster_name
  cluster_version = "1.30"

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
    }
  }
  
  tags = {
    Environment = terraform.workspace
    GithubRepo  = "terraform-aws-eks"
    GithubOrg   = "terraform-aws-modules"
  }

  vpc_id = module.vpc.vpc_id
  subnet_ids      = module.vpc.private_subnets

  eks_managed_node_groups = {
    blue = {
      min_size     = 1
      max_size     = 10
      desired_size = 1
      ami_type = "AL2_ARM_64"
      instance_types = ["t4g.medium"]
      capacity_type  = "ON_DEMAND"
    }
    green = {
      min_size     = 1
      max_size     = 10
      desired_size = 1
      ami_type = "AL2_ARM_64"
      instance_types = ["t4g.medium"]
      capacity_type  = "ON_DEMAND"
    }
  } 
}

