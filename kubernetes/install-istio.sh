#!/usr/bin/env bash

aws eks --region us-west-2 update-kubeconfig --name $1

echo "Getting istio........"
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.15.0 TARGET_ARCH=x86_64 sh -
cd istio-1.15.0
export PATH=$PWD/bin:$PATH
echo "Installing istio......"
istioctl install --set profile=demo
kubectl label namespace default istio-injection=enabled
kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml
kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
echo "Setting host variables......."
export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].port}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].port}')
export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT
echo "$GATEWAY_URL"
echo "Demo Application: $GATEWAY_URL"
echo "Page: /productpage"
echo "Applying addons......."
kubectl apply -f samples/addons
sleep 30
while ! kubectl wait --for=condition=available --timeout=600s deployment/kiali -n istio-system; do sleep 1; done
echo "Starting Kiali......."
istioctl dashboard kiali &
