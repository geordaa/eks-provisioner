terraform {
  required_version = "~> 1.5"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.17"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~> 2.31"
    }
    random = {
      source = "hashicorp/random"
      version = "~> 3.4"
    }
    vault = {
      source  = "hashicorp/vault"
      version = "~> 3.11"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.2"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 3.2"
    }
    template = {
      source  = "hashicorp/cloudinit"
      version = "~> 2.2"
    }
  }
}
