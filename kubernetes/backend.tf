terraform {  
  backend "s3" {
    bucket  = "geordaa-terraform-backend-store"
    encrypt = true
    key     = "kubernetes/terraform.tfstate"
    region  = "eu-west-2"
  }
}
